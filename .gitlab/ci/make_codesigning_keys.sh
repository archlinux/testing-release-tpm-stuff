#!/usr/bin/env bash

codesigning_dir="${PWD}/.codesigning/"
ca_dir="${codesigning_dir}/ca/"

ca_conf="${ca_dir}/certificate_authority.cnf"
ca_subj='/C=DE/ST=Berlin/L=Berlin/O=Arch Linux/OU=Release Engineering/emailAddress=arch-releng@lists.archlinux.org/CN=Arch Linux Release Engineering (Ephemeral Certificate Authority)'
ca_cert="${ca_dir}/cacert.pem"
ca_key="${ca_dir}/private/cakey.pem"

codesigning_conf="${codesigning_dir}/code_signing.cnf"
codesigning_subj='/C=DE/ST=Berlin/L=Berlin/O=Arch Linux/OU=Release Engineering/emailAddress=arch-releng@lists.archlinux.org/CN=Arch Linux Release Engineering (Ephemeral Signing Key)'
codesigning_cert="${codesigning_dir}/codesign.crt"
codesigning_key="${codesigning_dir}/codesign.key"

rm -rf "$codesigning_dir"
rm -f testkey.pem testcert.pem testcacert.pem

mkdir -p "${ca_dir}/"{private,newcerts,crl}
mkdir -p "${codesigning_dir}"
cp -- /etc/ssl/openssl.cnf "${codesigning_conf}"
cp -- /etc/ssl/openssl.cnf "${ca_conf}"
touch "${ca_dir}/index.txt"
echo "1000" >"${ca_dir}/serial"

# Prepare the ca configuration for the change in directory
sed -i "s#/etc/ssl#${ca_dir}#g" "${ca_conf}"

# Create the Certificate Authority
openssl req \
    -provider tpm2 -provider default -propquery '?provider=tpm2' \
    -newkey rsa:2048 \
    -nodes \
    -x509 \
    -new \
    -sha256 \
    -keyout "${ca_key}" \
    -config "${ca_conf}" \
    -subj "${ca_subj}" \
    -days 2 \
    -out "${ca_cert}"

IFS='' read -r -d '' extension_text <<EOF || true
[codesigning]
keyUsage=digitalSignature
extendedKeyUsage=codeSigning, clientAuth, emailProtection
EOF

printf '%s' "${extension_text}" >> "${ca_conf}"
printf '%s' "${extension_text}" >> "${codesigning_conf}"

openssl req \
    -provider tpm2 -provider default -propquery '?provider=tpm2' \
    -newkey rsa:2048 \
    -keyout "${codesigning_key}" \
    -nodes \
    -sha256 \
    -out "${codesigning_cert}.csr" \
    -config "${codesigning_conf}" \
    -subj "${codesigning_subj}" \
    -extensions codesigning

# Sign the code signing certificate with the CA
openssl ca \
    -provider tpm2 -provider default -propquery '?provider=tpm2' \
    -batch \
    -config "${ca_conf}" \
    -extensions codesigning \
    -days 2 \
    -notext \
    -md sha256 \
    -in "${codesigning_cert}.csr" \
    -out "${codesigning_cert}"

cp -- "$codesigning_key" testkey.pem
cp -- "$codesigning_cert" testcert.pem
cp -- "$ca_cert" testcacert.pem
